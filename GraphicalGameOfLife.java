import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.*;

/*	*******************	*/
/*	The testing class	*/
/*	*******************	*/

import java.awt.*;

import javax.swing.*;

public class GraphicalGameOfLife{
	static Automata goL;
	static GraphicDisplayer space;
	static JFrame screen = new JFrame();
	static JButton next,init,jump,run,pause, resize, respeed, pasB;
	static JPanel buttonPanel;
	static Boolean go;
	static int speed = 120, pas = 10;
	
	public static void main(String[] arg) {
	
		int dimCube = 10;
		int lDim = Integer.parseInt(JOptionPane.showInputDialog("Number of rows: "));
		int cDim = Integer.parseInt(JOptionPane.showInputDialog("Number of columns: "));
		goL = new Automata(lDim,cDim);
		goL.init(dimCube);
		space = new GraphicDisplayer(goL, dimCube);
		go = new Boolean(false);
		
		space.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent mouse) {
				space.requestFocus();
				if (mouse.getButton() == MouseEvent.BUTTON1) {
					goL.clicked(mouse.getX(), mouse.getY(), space.getStartX(), space.getStartY());
        			space.repaint();
				}
			}
		});
		
		space.addMouseWheelListener(new MouseAdapter() {
			public void mouseWheelMoved(MouseWheelEvent e) {
		        int notches = e.getWheelRotation();
		        int size = goL.getDimCube();
		        if (notches < 0) {
		        	goL.setDimCube(size+1);
					space.setDimCube(size+1);
					space.repaint();
		        } else {
		        	goL.setDimCube(size-1);
					space.setDimCube(size-1);
					space.repaint();
		        }
		    } 
		});
		
		space.setFocusable(true);
		space.requestFocus();
		
		space.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int x = space.getStartX();
				int y = space.getStartY();
				if(e.getKeyCode() == KeyEvent.VK_Z)
				{
					space.setStartX(x-pas);
					space.repaint();
				}
				if(e.getKeyCode() == KeyEvent.VK_S)
				{
					space.setStartX(x+pas);
					space.repaint();
				}
				if(e.getKeyCode() == KeyEvent.VK_D)
				{
					space.setStartY(y+pas);
					space.repaint();
				}
				if(e.getKeyCode() == KeyEvent.VK_Q)
				{
					space.setStartY(y-pas);
					space.repaint();
				}
				if(e.getKeyCode() == KeyEvent.VK_P)
				{
					speed -= 10;
				}
				if(e.getKeyCode() == KeyEvent.VK_M)
				{
					speed += 10;
				}
			}
		});

		next = new JButton("Next");
		next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goL.evolve();
				space.repaint();
			}
		}
		);

		init = new JButton("Init");
		init.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goL.init(dimCube);
				space.repaint();
			}
		}
		);

		jump = new JButton("Jump");
		jump.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int iter = Integer.parseInt(JOptionPane.showInputDialog("Leap: "));
				for (int i=0; i<iter; i++) goL.evolve();
				space.repaint();
			}
		}
		);
		
		resize = new JButton("resize");
		resize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int iter = Integer.parseInt(JOptionPane.showInputDialog("New size: "));
				goL.setDimCube(iter);
				space.setDimCube(iter);
				space.repaint();
			}
		}
		);
		
		pasB = new JButton("pas");
		pasB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pas = Integer.parseInt(JOptionPane.showInputDialog("New pas: "));
			}
		}
		);
		
		respeed = new JButton("respeed");
		respeed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				speed = Integer.parseInt(JOptionPane.showInputDialog("New speed: "));
			}
		}
		);

		run = new JButton("Run");
		run.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				class Process implements Runnable {
					boolean running;
					Process() {
						running = true;
					}
					public void run() {
						while (running) {
							synchronized(go) {
								if (go.booleanValue()) {
									goL.evolve();
									space.repaint();
									
								}
								else running = false;
							}
							try { Thread.sleep(speed);}
							catch (InterruptedException f) {}
						}
					}
				}
				synchronized(go) {
					go = new Boolean(true);
				}
				Thread me = new Thread(new Process());
				me.start();
			}
		}
		);

		pause = new JButton("Pause");
		pause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized(go) {
					go = new Boolean(false);
				}
			}
		}
		);
		
		buttonPanel = new JPanel();
		buttonPanel.add(init);
		buttonPanel.add(next);
		buttonPanel.add(jump);		
		buttonPanel.add(run);
		buttonPanel.add(pause);
		buttonPanel.add(resize);
		buttonPanel.add(respeed);
		buttonPanel.add(pasB);
		
		screen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		screen.setSize(800,600);
		screen.setLayout(new BorderLayout());
		screen.add(space,BorderLayout.CENTER);
		screen.add(buttonPanel,BorderLayout.SOUTH);
		screen.setVisible(true);
	}
	
}