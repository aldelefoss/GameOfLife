/*	***********************	*/
/*	The graphic displayer	*/
/*	***********************	*/

import java.awt.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class GraphicDisplayer extends JPanel {
	Automata myAutomata;
	int startX, startY, _size;
	
	public GraphicDisplayer(Automata d, int size){
		super();
		myAutomata = d;
		startX = 20;
		startY = 20;
		_size = size;
	}
	
	public int getStartX() {
		return startX;
	}

	public void setStartX(int startX) {
		this.startX = startX;
	}

	public int getStartY() {
		return startY;
	}

	public void setStartY(int startY) {
		this.startY = startY;
	}

	public void setDimCube(int dim) {
		_size = dim;
	}
	
	public int getDimCube() {
		return _size;
	}
	
	public void paintComponent(Graphics g)  {
		super.paintComponent(g);
		this.setBackground(Color.DARK_GRAY);
		for(int i=0; i<myAutomata.getWidth(); i++)  {
			for(int j=0; j<myAutomata.getHeight(); j++)  {
				if(myAutomata.isLiving(i,j)) {
					g.setColor(Color.decode("#FFD557"));
				}
				else  {
					g.setColor(Color.GRAY);
				}
				g.fillRect(startY+_size*j+j, startX+_size*i+i, _size, _size);
			}
		}
	}
}