/*	***********************	*/
/*	The cellular Automata	*/
/*	***********************	*/


public class Automata {
	protected int grid[][], clone[][], dimCube;

	public Automata(int lineNb, int columnNb) {
		grid= new int[lineNb][columnNb];
		clone = new int[lineNb][columnNb];
	}

	public void init(int dim) {
		//Random r = new Random();
		dimCube = dim;
		for (int i=0; i<grid.length; i++) {
			for (int j=0; j<grid[0].length; j++) {
				grid[i][j]= 0;//r.nextInt(2);
			}
		}
	}

	public void setDimCube(int dim) {
		dimCube = dim;
	}
	
	public int getDimCube() {
		return dimCube;
	}
	
	public int getWidth() {
		return grid.length;
	}
	
	public int getHeight() {
		return grid[0].length;
	}
	
	public boolean isLiving(int i, int j) {
		return (grid[i][j] == 1);
	}
	
	private int countLivingNeighbors(int i, int j) {
		int s=0;
		if (i>0) s=s+grid[i-1][j];
		if ((i>0) && (j<grid[0].length-1)) s=s+grid[i-1][j+1];
		if (j<grid[0].length-1) s=s+grid[i][j+1];
		if ((i<grid.length-1) && (j<grid[0].length-1)) s=s+grid[i+1][j+1];
		if (i<grid.length-1) s=s+grid[i+1][j];
		if ((i<grid.length-1) && (j>0)) s=s+grid[i+1][j-1];
		if (j>0) s=s+grid[i][j-1];
		if ((i>0) && (j>0)) s=s+grid[i-1][j-1];
		return s;
	}

	public void evolve() {
		int nbLivNgb;
		for (int i=0; i<grid.length; i++) {
			for (int j=0; j<grid[0].length; j++) {
				nbLivNgb=countLivingNeighbors(i,j);
				if (grid[i][j]==1) {
					if ((nbLivNgb<2)||(nbLivNgb>3)) clone[i][j]=0;
					else clone[i][j]=1;
				}
				else {
					if (nbLivNgb==3) clone[i][j]=1;
					else clone[i][j]=0;
				}
			}
		}
		for (int i=0; i<grid.length; i++) {
			for (int j=0; j<grid[0].length; j++) {
				grid[i][j]=clone[i][j];
			}
		}
	}
	public void clicked(int x, int y, int offy, int offx) {
		try {
			grid[(y-offy)/(dimCube+1)][(x-offx)/(dimCube+1)] = 1-grid[(y-offy)/(dimCube+1)][(x-offx)/(dimCube+1)];
			clone[(y-offy)/(dimCube+1)][(x-offx)/(dimCube+1)] = 1-clone[(y-offy)/(dimCube+1)][(x-offx)/(dimCube+1)];
		} catch (RuntimeException e) {
			java.lang.System.err.println("Mouse Click out of reach");
		}
	}
}